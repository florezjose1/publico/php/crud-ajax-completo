function selectLanguajes() {
    var cantCOnslta =  $('#numPage').val();
    console.log(cantCOnslta + ' =============000')
    var param ={'Funcion':'selectLanguajes', 'variable':'1' };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            console.log(data)
            var num = JSON.parse(data);
            // variable n guardamos la cantidad de registro que contiene nuestra DB
            var n = num[0].registros;
            console.log(n + ' numero de registros');

            var limit = $('#limitePagination').val();
            var param ={'Funcion':'selectLanguajes', 'limit':limit, 'canConsulta':cantCOnslta, 'variable':'2' };
            $.ajax({
                data: JSON.stringify(param),
                type:"JSON",
                url: 'ajax.php',
                success:function (data) {
                    //console.log(data)
                    var json = JSON.parse(data);

                    // variable page guardaremos el promedio redondeado arriba
                    //para saber la cantidad de paginas que tendran nuestra lista
                    var page = Math.ceil( parseInt(n) / parseInt(cantCOnslta) );

                    var a = '<li style="border: 1px solid #cacaca; " class="page-item"><a class="page-link" onclick="pagination(0)" href="#">1</a>';
                    for (i = 2; i <= page; i++) {
                        var cantC = parseInt(cantCOnslta) * ( parseInt(i) - 1);
                        a = a + '<li style="border: 1px solid #cacaca; " class="page-item"><a class="page-link" onclick="pagination('+ cantC + ')" href="#">'+i+'</a></li>';
                    }
                    $('#listPagination').html(a);

                    var opt = parseInt(cantCOnslta)* (parseInt(page) - parseInt(1));
                    if ( limit == opt ) {
                        $('#btSiguiente').hide('slow')
                    }else{
                        $('#btSiguiente').show('slow');

                    }

                    if (limit == 0 ) {
                        $('#btAnterior').hide('slow')
                    }else{
                        $('#btAnterior').show('slow');
                    }

                    //console.log('paginas' + page    )

                    var htm = "";
                    for (i = 0; i< json.length; i++) {

                        htm = htm + ' <tr> ' +
                            '<td>' + json[i].title  + '</td>' +
                            '<td>' + json[i].content  + '</td>' +

                            '<td>' +
                            '<a style=" font-size:20px; color: #22acff;" onclick="detalleLanguaje('+json[i]['id']+')">' +
                            '<i class=\"fa fa-edit\" aria-hidden=\"true\"></i>' +
                            '</a>' +
                            '</td>' +
                            '<td>' +
                            '<a style=" font-size:20px; color: red;" onclick="detalleDeleteLanguaje('+json[i]['id']+')">' +
                            '<i class=\"fa fa-trash\" aria-hidden=\"true\"></i>' +
                            '</a>' +
                            '</td>' +
                            '</tr>'
                    }

                    $("#contentable").html(htm);


                }
            });
        }
    });
}

function regis() {
    $('#btn-salida').hide();
    $('#forRegister').show('slow');
}
function cancelarRegister() {
    $('#resertRegister').click();
    $('#forRegister').hide('slow');
    $('#btn-salida').show();
}
function registrarLenguaje() {
    var lenguaje = $('#languaje').val();
    var contenido = $('#contenido').val();

    var param ={'Funcion':'registrarLenguaje', 'lenguaje':lenguaje, 'contenido':contenido };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            console.log(data)
            if (data != 0 ) {
                var menj = 'Processing! Please wait..';
                var head = 'Registrando';
                var texto = 'Se ha insertado un nuevo Registro';
                success(menj,head,texto);
                setTimeout(function(){
                    $('#resertRegister').click()
                    $('#forRegister').hide('slow');
                    $('#btn-salida').show();
                    selectLanguajes();
                },1500);
            }else{
                var title = 'Processing!';
                var ms = ' Please wait..';
                var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                error(title,ms,mensaje);
                $('#noActLanguaje').click();
            }
        }
    });
}

function anterior(){
    var cantCOnslta =  $('#numPage').val();
    var limit = $('#limitePagination').val();
     var t = parseInt(limit) - parseInt(cantCOnslta);
     $('#limitePagination').val(t);
     selectLanguajes();
}

function siguinte(){
    var cantCOnslta =  $('#numPage').val();
    var limit = $('#limitePagination').val();
    var t = parseInt(limit) + parseInt(cantCOnslta);
    $('#limitePagination').val(t);
    selectLanguajes();
}


    function pagination(cantC) {
        console.log(cantC + ' qqqqqqqqqqq')
        $('#limitePagination').val(cantC);
        selectLanguajes();
    }

    function detalleLanguaje(id) {
        var param ={'Funcion':'detalleLanguaje', 'id':id};
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            success: function (data) {
                console.log(data)
                var json = JSON.parse(data)

                $('#detalleLanguaje').click()
                $('#titleLangueje').click();
                $('#idLanguaje').val(json[0].id)
                $('#titleLangueje').val(json[0].title)
                $('#contenidoLangueje').val(json[0].content)


            }
        });
    }

    function detalleDeleteLanguaje(id) {
        var param ={'Funcion':'detalleLanguaje', 'id':id};
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            success: function (data) {
                console.log(data)
                var json = JSON.parse(data)

                $('#ModalDeleteLanguaje').click()

                $('#idDelLanguaje').val(json[0].id)
                $('#languajeDetalle').html(json[0].title)


            }
        });
    }
    function deleteLanguaje() {
        var id = $('#idDelLanguaje').val();
        var param ={'Funcion':'deleteLanguaje', 'id':id };
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            success: function (data) {
                console.log(data)
                if (data != 0 ) {
                    var menj = 'Processing! Please wait..';
                    var head = 'Eliminado';
                    var texto = 'EL registro ha sido Eliminado';
                    success(menj,head,texto);
                    setTimeout(function(){
                        $('#noDeleteLang').click();
                        selectLanguajes();
                    },1500);
                }else{
                    var title = 'Processing!';
                    var ms = ' Please wait..';
                    var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                    error(title,ms,mensaje);
                    $('#noActLanguaje').click();
                }
            }
        });
    }

    function  actualizarLanguaje() {
        var id = $('#idLanguaje').val();
        var title = $('#titleLangueje').val();
        var content = $('#contenidoLangueje').val();
        var param ={'Funcion':'actualizarLanguaje', 'id':id, 'title':title, 'content':content};
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            success: function (data) {
                console.log(data)
                if (data != 0 ) {
                    var menj = 'Processing! Please wait..';
                    var head = 'Actualizado';
                    var texto = 'EL registro se ha actualizado';
                    success(menj,head,texto);
                    setTimeout(function(){
                        $('#noActLanguaje').click();
                        $('#cerrarUpLanguaje').click();
                        selectLanguajes();
                    },1500);
                }else{
                    var title = 'Processing!';
                    var ms = ' Please wait..';
                    var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                    error(title,ms,mensaje);
                    $('#noActLanguaje').click();
                }
            }
        });
    }

    
    function success(menj, head, texto) {
        $.toast({
            text: menj,
            textAlign: 'center',
            bgColor: '#1a6eff',
            position: 'top-right'
        })
        setTimeout(function(){
            $.toast({
                heading: head,
                text: texto,
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'success'
            })
        },2000);


    }

    function error(title, ms, mensaje) {
        var myToast = $.toast({
            heading: title,
            text: ms,
            icon: 'info',
            position: 'top-right',
            hideAfter: false
        });

        // Update the toast after three seconds.
        window.setTimeout(function(){
            myToast.update({
                heading: 'Error',
                text: mensaje,
                icon: 'error',
                position: 'top-right',
                hideAfter: false,
                showHideTransition: 'slide',
            });
        }, 2000)
    }