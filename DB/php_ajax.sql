-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-03-2017 a las 13:53:23
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `php_ajax`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lenguajes`
--

CREATE TABLE `lenguajes` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lenguajes`
--

INSERT INTO `lenguajes` (`id`, `title`, `content`) VALUES
(3, 'PHP', 'Tempor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus.'),
(5, 'de php', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(6, 'JAVASCRIPT', '<p>ortor mi phasellus tellus mauris vehicula, et dui, cras vivamus donec vehicula convallis, quis volutpat, nunc dignissim ped</p>'),
(7, 'ANGULAR juan', '<p>Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus </p>'),
(8, 'RUBY', '<p>Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur risus aliquam quis. Felis luctus felis mus dapibus lobortis, sed faucibus libero officia libero</p>'),
(9, 'PERL', '<p>s et scelerisque, lectus nulla voluptatem lectus in. Ipsum cras in lectus massa leo, ut interdum nascetur magna vestibulum, suspendisse eligendi consectetuer magna wisi vestibulum cras. Bibendum eleifend, lacus in, in quam suspendisse. Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur ri</p>'),
(10, 'ORACLE', '<p> consectetuer, cursus est accumsan. Fermentum iaculis diam amet, ipsum velit a enim velit, et eu, vitae ullamcorper, et cras sit tortor massa urna orci. Metus sodales rutrum ante sem, aliquam dui mollis nonummy. Orci eleifend turpis quam cum faucibus, in q</p>'),
(11, 'MYSQL', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(12, 'POSTGRES', '<p>Vitae vivamus cursus, in et erat sed. Turpis erat elit facilisis, faucibus eget. </p>'),
(13, 'VISUAL BASIC', '<p>Vestibulum placerat consectetuer, accusantium liber</p>'),
(14, 'JQUERY', 'Cras sodales suspendisse dui luctus enim, tempus imperdiet ridiculus.'),
(15, 'R ', ' empor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus, sociis id quam mauris, ac condimentum ad, amet interdum ut varius lobortis. Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus nulla euismod arcu, augue purus ut elit pulvinar, urna mattis suscipit justo orci '),
(16, 'JQUERY', '<p>nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla</p>'),
(17, 'JAVASCRIPT', 'Lenguaje versÃ¡til con una comodidades.'),
(20, 'Html', 'nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla'),
(21, 'PHP', 'Tempor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus.'),
(22, 'de php', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(23, 'JAVASCRIPT', '<p>ortor mi phasellus tellus mauris vehicula, et dui, cras vivamus donec vehicula convallis, quis volutpat, nunc dignissim ped</p>'),
(24, 'ANGULAR juan', '<p>Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus </p>'),
(25, 'RUBY', '<p>Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur risus aliquam quis. Felis luctus felis mus dapibus lobortis, sed faucibus libero officia libero</p>'),
(26, 'PERL', '<p>s et scelerisque, lectus nulla voluptatem lectus in. Ipsum cras in lectus massa leo, ut interdum nascetur magna vestibulum, suspendisse eligendi consectetuer magna wisi vestibulum cras. Bibendum eleifend, lacus in, in quam suspendisse. Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur ri</p>'),
(27, 'ORACLE', '<p> consectetuer, cursus est accumsan. Fermentum iaculis diam amet, ipsum velit a enim velit, et eu, vitae ullamcorper, et cras sit tortor massa urna orci. Metus sodales rutrum ante sem, aliquam dui mollis nonummy. Orci eleifend turpis quam cum faucibus, in q</p>'),
(28, 'MYSQL', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(29, 'POSTGRES', '<p>Vitae vivamus cursus, in et erat sed. Turpis erat elit facilisis, faucibus eget. </p>'),
(30, 'VISUAL BASIC', '<p>Vestibulum placerat consectetuer, accusantium liber</p>'),
(31, 'JQUERY', 'Cras sodales suspendisse dui luctus enim, tempus imperdiet ridiculus.'),
(32, 'R ', ' empor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus, sociis id quam mauris, ac condimentum ad, amet interdum ut varius lobortis. Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus nulla euismod arcu, augue purus ut elit pulvinar, urna mattis suscipit justo orci '),
(33, 'JQUERY', '<p>nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla</p>'),
(34, 'JAVASCRIPT', 'Lenguaje versÃ¡til con una comodidades.'),
(35, 'Html', 'nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla'),
(36, 'PHP', 'Tempor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus.'),
(37, 'de php', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(38, 'JAVASCRIPT', '<p>ortor mi phasellus tellus mauris vehicula, et dui, cras vivamus donec vehicula convallis, quis volutpat, nunc dignissim ped</p>'),
(39, 'ANGULAR juan', '<p>Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus </p>'),
(40, 'RUBY', '<p>Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur risus aliquam quis. Felis luctus felis mus dapibus lobortis, sed faucibus libero officia libero</p>'),
(41, 'PERL', '<p>s et scelerisque, lectus nulla voluptatem lectus in. Ipsum cras in lectus massa leo, ut interdum nascetur magna vestibulum, suspendisse eligendi consectetuer magna wisi vestibulum cras. Bibendum eleifend, lacus in, in quam suspendisse. Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur ri</p>'),
(42, 'ORACLE', '<p> consectetuer, cursus est accumsan. Fermentum iaculis diam amet, ipsum velit a enim velit, et eu, vitae ullamcorper, et cras sit tortor massa urna orci. Metus sodales rutrum ante sem, aliquam dui mollis nonummy. Orci eleifend turpis quam cum faucibus, in q</p>'),
(43, 'MYSQL', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(44, 'POSTGRES', '<p>Vitae vivamus cursus, in et erat sed. Turpis erat elit facilisis, faucibus eget. </p>'),
(45, 'VISUAL BASIC', '<p>Vestibulum placerat consectetuer, accusantium liber</p>'),
(46, 'JQUERY', 'Cras sodales suspendisse dui luctus enim, tempus imperdiet ridiculus.'),
(47, 'R ', ' empor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus, sociis id quam mauris, ac condimentum ad, amet interdum ut varius lobortis. Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus nulla euismod arcu, augue purus ut elit pulvinar, urna mattis suscipit justo orci '),
(48, 'JQUERY', '<p>nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla</p>'),
(50, 'Html', 'nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla'),
(51, 'PHP', 'Tempor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus.'),
(52, 'de php', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(53, 'JAVASCRIPT', '<p>ortor mi phasellus tellus mauris vehicula, et dui, cras vivamus donec vehicula convallis, quis volutpat, nunc dignissim ped</p>'),
(54, 'ANGULAR juan', '<p>Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus </p>'),
(55, 'RUBY', '<p>Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur risus aliquam quis. Felis luctus felis mus dapibus lobortis, sed faucibus libero officia libero</p>'),
(56, 'PERL', '<p>s et scelerisque, lectus nulla voluptatem lectus in. Ipsum cras in lectus massa leo, ut interdum nascetur magna vestibulum, suspendisse eligendi consectetuer magna wisi vestibulum cras. Bibendum eleifend, lacus in, in quam suspendisse. Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur ri</p>'),
(57, 'ORACLE', '<p> consectetuer, cursus est accumsan. Fermentum iaculis diam amet, ipsum velit a enim velit, et eu, vitae ullamcorper, et cras sit tortor massa urna orci. Metus sodales rutrum ante sem, aliquam dui mollis nonummy. Orci eleifend turpis quam cum faucibus, in q</p>'),
(58, 'MYSQL', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>'),
(59, 'POSTGRES', '<p>Vitae vivamus cursus, in et erat sed. Turpis erat elit facilisis, faucibus eget. </p>');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `lenguajes`
--
ALTER TABLE `lenguajes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `lenguajes`
--
ALTER TABLE `lenguajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
