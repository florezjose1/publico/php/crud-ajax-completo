<?php

require_once 'conexion.php';

class PrincipalController extends Conexion {
    
    // funciones para ejecutar nuestras consultas sql
   	public function selectLanguajes($request) {
   	    $variable = $request->variable;

   	    if ($variable == 1 ) {
            $query = "SELECT count(`id`) as registros FROM `lenguajes`";
            $result = $this->EjecutarPhpAjax($query);

            $post = array();
            while ($row = mysqli_fetch_object($result)) {
                $post[]= array( 'registros'=>$row->registros  );
            }
            echo json_encode($post);
        }

        if ($variable == 2 ) {
            $limit = $request->limit;
            $canConsul = $request->canConsulta;
            $query = "SELECT `id`, `title`, `content` FROM `lenguajes` ORDER BY id DESC LIMIT $limit,$canConsul  ";
            $result = $this->EjecutarPhpAjax($query);

            $post = array();
            while ($row = mysqli_fetch_object($result)) {
                $post[]= array( 'id'=>$row->id, 'title'=>$row->title, 'content'=>$row->content  );
            }
            echo json_encode($post);
        }



   	}

    public function registrarLenguaje($request) {

        $lenguaje = $request->lenguaje;
        $contenido = $request->contenido;

        $query = "INSERT INTO `lenguajes`( `title`, `content`) VALUES ('$lenguaje','$contenido' )  ";
        $result = $this->EjecutarPhpAjax($query);

        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }


    public function detalleLanguaje($request) {

   	    $id = $request->id;

        $query = "SELECT `id`, `title`, `content` FROM `lenguajes` WHERE id = '$id'  ";
        $result = $this->EjecutarPhpAjax($query);

        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id'=>$row->id, 'title'=>$row->title, 'content'=>$row->content  );
        }
        echo json_encode($post);
    }

    public function actualizarLanguaje($request) {

        $id = $request->id;
        $title = $request->title;
        $content = $request->content;

        $query = "UPDATE `lenguajes` SET `title` = '$title', `content` =  '$content' WHERE  `id` = '$id'  ";
        $result = $this->EjecutarPhpAjax($query);

        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }

    public function deleteLanguaje($request) {

        $id = $request->id;
        $query = "DELETE FROM `lenguajes` WHERE   `id` = '$id'  ";
        $result = $this->EjecutarPhpAjax($query);

        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }


}
