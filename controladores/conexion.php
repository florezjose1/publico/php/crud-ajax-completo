<?php


require_once 'vars.php'; // requerimos el archivo de variables que hemos creado
class Conexion extends Variables{ // hacemo una clase llamada conexion y le referenciamos las variables.

    
   	function __Construct()
    {
    	# code...
        session_start();
        date_default_timezone_set('America/Bogota');
    	$var   = new Variables();
    }
    
    // funcion publica en el cual lo que haremos será conectar a nuestra Db
    public function ConectarPhpAjax(){
        $connect = mysqli_connect($this->host,$this->user,$this->pass,$this->dbPhpAjax) // al final de la linea es donde llamamos a la variable que hemos creado en vars.php
            or die ("Error". mysqli_error($connect));
        return $connect;   
    }
    
    // y ejecutamos la conecion... el ejecutarPruebas es el que utilizaremos para cuando hagamos nuestras consultas sql en el principalControllers
    public function EjecutarPhpAjax($sql){
        $connect = $this->ConectarPhpAjax(); //llamamos a la funcion que hemos creado de conectar a nuestra DB
        $result = $connect->query($sql);
        return $result;
    }
    
   
}

?>
